export const eventBus = {
    /**
     *
     * @param {string} event
     * @param {Function} callback
     */
    on(event, callback) {
      document.addEventListener(
        event,
        (e) => e.stopImmediatePropagation() | callback(e.detail)
      );
    },
    /**
     *
     * @param {string} event
     * @param {*} data
     */
    dispatch(event, data) {
      document.dispatchEvent(new CustomEvent(event, { detail: data }));
    },
    /**
     *
     * @param {string} event
     * @param {Function} callback
     */
    remove(event, callback) {
      document.removeEventListener(event, callback);
    },
  };