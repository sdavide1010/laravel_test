<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Inertia\Inertia;

class UserController extends Controller
{
    //    
    private function get_fk_table($table_name, $column_name = '')
    {
        
        return array_map(
            fn ($v) => $column_name === 'col' ? $v->c_fk : ($column_name === 'tab' ? $v->t_fk : $v),
            DB::select("with list_foreign as         
        (select REFERENCED_TABLE_NAME as t_fk, COLUMN_NAME as c_fk 
       from INFORMATION_SCHEMA.KEY_COLUMN_USAGE where TABLE_NAME='{$table_name}') 
       select " . ($column_name === 'col' ? 'c_fk' : ($column_name === 'tab' ? 't_fk' : '*'))
                . " from list_foreign where not t_fk='NULL' ;")
        );
    }
    public function get_edit_data_user()
    {
        return Inertia::render(
            'EditUsers',
            [
                'describeTable' => DB::select('DESCRIBE users'),
                // 'dataDepartments' => DB::table('departments')->get(),
                // 'dataMunicipalities' => DB::table('municipalities')->get(),
                'userInfoFk' => array_map(
                    fn ($v) => [$v->c_fk => ['colFk'=>$this->get_fk_table($v->t_fk,'col'),
                    'data'=>DB::table($v->t_fk)->get()]],
                    $this->get_fk_table('users')
                ),
                'users' => DB::table('users')->get()
            ]

        );
    }
    public function update_extra_data(Request $request, User $user)
    {
              $user->update($request->all());
              return redirect()->route('editusers');
      
    }
}
