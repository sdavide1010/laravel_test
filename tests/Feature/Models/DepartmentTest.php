<?php

namespace Tests\Feature\Models;

use App\Http\Controllers\DepartmentController;
use App\Models\Department;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class DepartmentTest extends TestCase
{
    use RefreshDatabase;
    /**
     * Addded Department
     *
     * @return void
     */
    public function test_create_and_get_department()
    {
        Department::factory(10)->create();        
        $response = Department::all() ;       
        $this->assertTrue(count($response)==10);
        
    }
}
