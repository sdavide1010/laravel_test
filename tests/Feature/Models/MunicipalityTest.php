<?php

namespace Tests\Feature\Models;

use App\Models\Department;
use App\Models\Municipality;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class MunicipalityTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_create_and_get_municipality()
    {
        Department::factory(10)->create();
        Municipality::factory(10)->create();        
        $response = Municipality::all() ;       
        $this->assertTrue(count($response)==10);
    }
}
