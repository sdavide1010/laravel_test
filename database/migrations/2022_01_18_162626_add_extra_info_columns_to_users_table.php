<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddExtraInfoColumnsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            //
            $table->date('date_birth')->nullable();
            $table->string('name_emergency_contact')->nullable();
            $table->bigInteger('phone_emergency_contact')->nullable();
            $table->string('race')->nullable();
            $table->string('residence_address')->nullable();
            $table->unsignedBigInteger('department_id')->nullable();
            $table->foreign('department_id')->references('id')->on('departments')->onDelete('cascade')->nullable();
            $table->unsignedBigInteger('municipalities_id')->nullable();
            $table->foreign('municipalities_id')->references('id')->on('municipalities')->onDelete('cascade')->nullable();
            $table->json('childrens_information')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
