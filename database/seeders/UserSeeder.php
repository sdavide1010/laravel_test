<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::insert([[
            'name' => 'admin',
            'email' => 'admin@gmail.com',
            'password' => Hash::make('userpass123456'),
            'created_at' => now(),
            'updated_at' => now()
        ], [
            'name' => 'admin2',
            'email' => 'admin2@gmail.com',
            'password' => Hash::make('userpass123456'),
            'created_at' => now(),
            'updated_at' => now()
        ]]);
    }
}
