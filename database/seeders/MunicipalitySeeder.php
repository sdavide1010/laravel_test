<?php

namespace Database\Seeders;

use App\Models\Municipality;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class MunicipalitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        Municipality::insert([
            ['department_id' => 1, 'name' => 'Cali', 'created_at' => now(), 'updated_at' => now()],
            ['department_id' => 2, 'name' => 'Tabio', 'created_at' =>now(), 'updated_at' => now()],
            ['department_id' => 2, 'name' => 'Bogotá', 'created_at' => now(), 'updated_at' => now()],
            ['department_id' => 3, 'name' => 'Medellín', 'created_at' => now(), 'updated_at' => now()],
            ['department_id' => 3, 'name' => 'Yolombó', 'created_at' => now(), 'updated_at' => now()],
            ['department_id' => 4, 'name' => 'Tunja', 'created_at' => now(), 'updated_at' => now()],
            ['department_id' => 4, 'name' => 'Paipa', 'created_at' => now(), 'updated_at' => now()]
        ]);
    }
}
