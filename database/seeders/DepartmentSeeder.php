<?php

namespace Database\Seeders;

use App\Models\Department;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\DB;

class DepartmentSeeder extends Seeder
{
    use RefreshDatabase;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Department::insert([
            ['name' => 'Valle del Cauca', 'created_at' => now(), 'updated_at' => now()],
            ['name' => 'Cundinamarca', 'created_at' => now(), 'updated_at' => now()],
            ['name' => 'Antioquia', 'created_at' => now(), 'updated_at' => now()],
            ['name' => 'Boyacá', 'created_at' => now(), 'updated_at' => now()]
        ]);
    }
}
